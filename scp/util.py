#!/usr/bin/env python3
#
##########################################################################
# Copyright (C) 2019 Mark J. Blair, NF6X
#
# This file is part of pySuperCardPro.
#
#  pySuperCardPro is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pySuperCardPro is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pySuperCardPro.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""Utility functions, mostly for internal use."""

def le32tbl(a):
    """Interpret byte array as array of 32 bit little-endian integers."""
    assert (len(a) >= 4)
    assert ((len(a) % 4) == 0)
    r = []
    for v in [a[n:n+4] for n in range(0, len(a), 4)]:
        r.append(int().from_bytes(v, byteorder='little', signed=False))
    return r


def le16tbl(a):
    """Interpret byte array as array of 16 bit little-endian integers."""
    assert (len(a) >= 2)
    assert ((len(a) % 2) == 0)
    r = []
    for v in [a[n:n+2] for n in range(0, len(a), 2)]:
        r.append(int().from_bytes(v, byteorder='little', signed=False))
    return r


def be32tbl(a):
    """Interpret byte array as array of 32 bit big-endian integers."""
    assert (len(a) >= 4)
    assert ((len(a) % 4) == 0)
    r = []
    for v in [a[n:n+4] for n in range(0, len(a), 4)]:
        r.append(int().from_bytes(v, byteorder='big', signed=False))
    return r


def be16tbl(a):
    """Interpret byte array as array of 16 bit big-endian integers."""
    assert (len(a) >= 2)
    assert ((len(a) % 2) == 0)
    r = []
    for v in [a[n:n+2] for n in range(0, len(a), 2)]:
        r.append(int().from_bytes(v, byteorder='big', signed=False))
    return r


def indent(text, indent=4):
    """Indent lines of text by specified number of spaces."""

    return '\n'.join(' '*indent + line for line in text.splitlines()) + '\n'

