#!/usr/bin/env python3
#
##########################################################################
# Copyright (C) 2019 Mark J. Blair, NF6X
#
# This file is part of pySuperCardPro.
#
#  pySuperCardPro is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pySuperCardPro is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pySuperCardPro.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""Main code for scputil script."""

import argparse
import textwrap
import scp
from scp.scputil_cmds import *


# Argument type validators

def drive_type(arg):
    """Validate drive letter argument."""
    if arg.upper() in scp.limits['drive']:
        return arg.upper()
    else:
        raise argparse.ArgumentTypeError('Drive letter must be in {:s}.'.format(str(scp.limits['drive'])))
    

def side_type(arg):
    """Validate side number argument.

    For the command line arguments, I use:
       0 = bottom (only side of single-sided disks)
       1 = top    (second side of double-sided disks)
       2 = both sides

    The SCP hardware uses:
       0 = bottom (only side of single-sided disks)
       1 = top    (second side of double-sided disks)

    The SCP file format uses:
       0 = both sides
       1 = bottom (only side of single-sided disks)
       2 = top    (second side of double-sided disks)

    Thus, the hardware and file format were already inconsistent before I
    added a third mapping for the command-line arguments."""
    n = int(arg, 0)
    if n in [0, 1, 2]:
        return n
    else:
        raise argparse.ArgumentTypeError('Side number must be 0, 1, or 2.')

    
def cyl_type(arg):
    """Validate cylinder number argument."""
    n = int(arg, 0)
    if n in scp.limits['cyl']:
        return n
    else:
        raise argparse.ArgumentTypeError('Cylinder number must be in range {:d}-{:d}.'.format(
            min(scp.limits['cyl']), max(scp.limits['cyl'])))


def rpm_type(arg):
    """Validate drive RPM argument."""
    n = int(arg, 0)
    if n in scp.limits['rpm']:
        return n
    else:
        raise argparse.ArgumentTypeError('Drive RPM must be in {:s}.'.format(str(scp.limits['rpm'])))

    
def tpi_type(arg):
    """Validate drive tracks per inch argument."""
    n = int(arg, 0)
    if n in scp.limits['tpi']:
        return n
    else:
        raise argparse.ArgumentTypeError('TPI must be in {:s}.'.format(str(scp.limits['tpi'])))


def revs_type(arg):
    """Validate number of revolutions argument."""
    n = int(arg, 0)
    if n in scp.limits['nrevs']:
        return n
    else:
        raise argparse.ArgumentTypeError('Revolution count must be in range {:d}-{:d}.'.format(
            min(scp.limits['nrevs']), max(scp.limits['nrevs'])))


def cmd_type(arg):
    """Validate command argument."""
    if arg.lower() in cmdtable:
        return arg.lower()
    else:
        raise argparse.ArgumentTypeError('Unknown command. Valid commands are {:s}.'.format(str(list(cmdtable.keys()))))


class fiveinch_action(argparse.Action):
    """Set defaults for 5.25 inch 40 track drives"""
    def __init__(self, option_strings, dest, **kwargs):
        super(fiveinch_action, self).__init__(option_strings, dest=argparse.SUPPRESS, nargs=0, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, 'endcyl', 39)
        setattr(namespace, 'rpm',    300)
        setattr(namespace, 'tpi',    48)


class eightinch_action(argparse.Action):
    """Set defaults for 8 inch drives"""
    def __init__(self, option_strings, dest, **kwargs):
        super(eightinch_action, self).__init__(option_strings, dest=argparse.SUPPRESS, nargs=0, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, 'endcyl', 76)
        setattr(namespace, 'rpm',    360)
        setattr(namespace, 'tpi',    48)


# Main function

def scputil_main():
    """main() function for scputil.py script.

    scputil.py is simply a stub that calls this function.
    Placing the code in here makes unit tests and code
    coverage measurement easier.

    Returns code that should be passed to sys.exit() by caller.
    0 implies success; nonzero or a string implies an error.

    Since this expects to be treated like a main() function
    in a script, it looks for command line arguments in sys.argv
    like any command line script would."""

    # Set up the command-line argument parser
    epilog = 'Commands:\n'
    for cmd in cmdtable:
        epilog = epilog + '  {:s}\n'.format(cmdtable[cmd].example)

    epilog = epilog + textwrap.dedent("""\

        Examples:
          scputil -8 clean 3
          scputil -8 -S 0 -c 'Single-sided 8in disk' read foo.scp
          scputil info foo.scp
          scputil -c 'A better comment' edit foo.scp bar.scp""")

    parser = argparse.ArgumentParser(
        prog='scputil',
        description=textwrap.dedent("""\
        SuperCard Pro utility version {:s}
          {:s}
          {:s}""".format(scp.__version__, scp.__copyright__, scp.__pkg_url__)),
        epilog=epilog,
        add_help=False,
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('-h', '--help', action='help',
                        help='Print this help message and exit.')

    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s {:s}'.format(scp.__version__),
                        help='Print version and exit.')

    parser.add_argument('-V', '--verbose', action='store_true', default=False,
                        help='Print status information while operating. Intended for use by the end user.')

    parser.add_argument('-D', '--debug', action='store_true', default=False,
                        help='Print debugging information while operating. Intended for use by the developer.')
    
    parser.add_argument('-p', '--port', metavar='PORT', default=scp.defaults['port'],
                        help='Serial port for SCP hardware (default {:s}).'.format(scp.defaults['port']))

    parser.add_argument('-d', '--drive', metavar='DRIVE', type=drive_type,
                        default=scp.defaults['drive'],
                        help='Drive to use: A or B (default {:s}).'.format(scp.defaults['drive']))

    parser.add_argument('-S', '--side', metavar='SIDE', type=side_type,
                        default=scp.defaults['side'],
                        help='Side(s) to image: 0=bottom, 1=top, 2=both (default {:d}).'.format(scp.defaults['side']))

    parser.add_argument('-s', '--startcyl', metavar='CYL', type=cyl_type,
                        default=scp.defaults['startcyl'],
                        help='Starting cylinder: 0-83 (default {:d}).'.format(scp.defaults['startcyl']))

    parser.add_argument('-e', '--endcyl', metavar='CYL', type=cyl_type,
                        default=scp.defaults['endcyl'],
                        help='Ending cylinder: 0-83 (default {:d}).'.format(scp.defaults['endcyl']))

    parser.add_argument('-r', '--rpm', metavar='RPM', type=rpm_type,
                        default=scp.defaults['rpm'],
                        help='Nominal drive RPM: 300 or 360 (default {:d}).'.format(scp.defaults['rpm']))

    parser.add_argument('-t', '--tpi', metavar='TPI', type=tpi_type,
                        default=scp.defaults['tpi'],
                        help='Tracks per inch: 48 or 96 (default {:d}).'.format(scp.defaults['tpi']))

    parser.add_argument('-n', '--nrevs', metavar='REVS', type=revs_type,
                        default=scp.defaults['nrevs'],
                        help='Number of revolutions to image: 1-5 (default {:d}).'.format(scp.defaults['nrevs']))

    parser.add_argument('-i', '--ignoreindex', action='store_true', default=False,
                        help='Ignore the index pulse when reading disks.')

    parser.add_argument('-z', '--suppress0seek', action='store_true', default=False,
                        help='Suppress seek to cylinder 0 in read/write/seek commands,' +
                             ' e.g. to avoid a damaged area of the disk.' +
                             ' Use seek command without -z on another disk first, to get SCP in' +
                             ' sync with the drive heads.')

    parser.add_argument('-c', '--comment', metavar='COMMENT', default=None,
                        help='Image file comment string (default none).')

    parser.add_argument('--drive_mfg', metavar='MANUFACTURER', default=None,
                        help='Drive manufacturer string (default none).')

    parser.add_argument('--drive_model', metavar='MODEL', default=None,
                        help='Drive model string (default none).')

    parser.add_argument('--drive_sernum', metavar='SERNUM', default=None,
                        help='Drive serial number string (default none).')

    parser.add_argument('--user', metavar='USER', default=None,
                        help='User name string (default none).')

    parser.add_argument('-5', '--fiveinch', action=fiveinch_action,
                        help='Convenience flag for 5.25" 40 track drives. Equivalent to: -e 39 -r 300 -t 48')

    parser.add_argument('-8', '--eightinch', action=eightinch_action,
                        help='Convenience flag for 8" drives. Equivalent to: -e 76 -r 360 -t 48')

    parser.add_argument('cmd', metavar='COMMAND', type=cmd_type, help='command to execute')

    parser.add_argument('cmdargs', metavar='ARG', nargs='*', help='arguments to command')


    # Parse the command-line arguments
    args = parser.parse_args()

    # Execute the specified command
    return cmdtable[args.cmd].fn(args)
