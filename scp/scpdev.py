#!/usr/bin/env python3
#
##########################################################################
# Copyright (C) 2019 Mark J. Blair, NF6X
#
# This file is part of pySuperCardPro.
#
#  pySuperCardPro is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pySuperCardPro is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pySuperCardPro.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""SuperCard Pro hardware device support.

Complies with API defined in:
  SuperCard Pro Software Developer's Kit Manual v1.7
  http://www.cbmstuff.com/downloads/scp/scp_sdk.pdf

SDK manual not included in this package due to license restriction."""

import serial
import scp
from scp.util import *
from scp import SCPfile


# Command bytes: Not exhaustive; See SCP SDK document for unused
# and unimplemented commands.
CMD_SELA        = 0x80
CMD_SELB        = 0x81
CMD_DSELA       = 0x82
CMD_DSELB       = 0x83
CMD_MTRAON      = 0x84
CMD_MTRBON      = 0x85
CMD_MTRAOFF     = 0x86
CMD_MTRBOFF     = 0x87
CMD_SEEK0       = 0x88
CMD_STEPTO      = 0x89
CMD_STEPIN      = 0x8A
CMD_STEPOUT     = 0x8B
CMD_SELDENS     = 0x8C
CMD_SIDE        = 0x8D
CMD_STATUS      = 0x8E
CMD_GETPARAMS   = 0x90
CMD_SETPARAMS   = 0x91
CMD_RAMTEST     = 0x92
CMD_READFLUX    = 0xA0
CMD_GETFLUXINFO = 0xA1
CMD_WRITEFLUX   = 0xA2
CMD_RAM2USB     = 0xA9
CMD_USB2RAM     = 0xAA
CMD_SCPINFO     = 0xD0

# Packet response codes
PR_UNUSED       = 0X00
PR_BADCOMMAND   = 0X01
PR_COMMANDERR   = 0X02
PR_CHECKSUM     = 0X03
PR_TIMEOUT      = 0X04
PR_NOTRK0       = 0X05
PR_NODRIVESEL   = 0X06
PR_NOMOTORSEL   = 0X07
PR_NOTREADY     = 0X08
PR_NOINDEX      = 0X09
PR_ZEROREVS     = 0X0A
PR_READTOOLONG  = 0X0B
PR_BADLENGTH    = 0X0C
PR_RESERVED1    = 0X0D
PR_BOUNDARYODD  = 0X0E
PR_WPENABLED    = 0X0F
PR_BADRAM       = 0X10
PR_NODISK       = 0X11
PR_BADBAUD      = 0X12
PR_BADCMDONPORT = 0X13
PR_OK           = 0X4F

pr_strings = {
    PR_UNUSED:       'Unused Response Code. You should never see this.',
    PR_BADCOMMAND:   'Bad command.',
    PR_COMMANDERR:   'Command error.',
    PR_CHECKSUM:     'Bad packet checksum.',
    PR_TIMEOUT:      'USB timeout.',
    PR_NOTRK0:       'Track 0 not found.',
    PR_NODRIVESEL:   'No drive selected.',
    PR_NOMOTORSEL:   'Motor not enabled.',
    PR_NOTREADY:     'Drive not ready.',
    PR_NOINDEX:      'No index pulse detected.',
    PR_ZEROREVS:     'Zero revolutions chosen.',
    PR_READTOOLONG:  'Read data too long for RAM.',
    PR_BADLENGTH:    'Invalid length value.',
    PR_RESERVED1:    'Reserved for future use.',
    PR_BOUNDARYODD:  'Location boundary is odd.',
    PR_WPENABLED:    'Diskette is write protected.',
    PR_BADRAM:       'RAM test failed.',
    PR_NODISK:       'No diskette in drive.',
    PR_BADBAUD:      'Bad baud rate selected.',
    PR_BADCMDONPORT: 'Command is not available for this type of port.',
    PR_OK:           'Success!'
}

class SCPdev():
    def __init__(self, port):
        pass
        self._port=serial.Serial(port, scp.defaults['baud'])

    def _getrsp(self, cmd, expect=None):
        """Get response packet from scp.

        cmd    = command
        expect = optional list of expected response codes"""
        rsp = self._port.read(2)
        if len(rsp) != 2:
            raise
        if rsp[0] != cmd:
            raise RuntimeError('Unexpected command reply 0x{:02X}.'.format(rsp[0]))
        if expect is None:
            # No expected responses supplied; raise exception if not PR_OK
            if rsp[1] != PR_OK:
                if rsp[1] in pr_strings:
                    raise RuntimeError('ERROR: {:s}'.format(pr_strings[rsp[1]]))
                else:
                    raise RuntimeError('ERROR: 0x{:02X}.'.format(rsp[1]))
        else:
            # Raise exception if this is an unexpected response
            if rsp[1] not in expect:
                if rsp[1] in pr_strings:
                    rstr = ' ({:s})'.format(pr_strings[rsp[1]])
                else:
                    rstr = ''
                raise RuntimeError('Unexpected response code 0x{:02X}{:s}.'.format(rsp[1], rstr))
        return rsp[1]


    def _sendcmd(self, cmd, payload=b'', expect=None, datalen=0, data=None):
        """Send command packet to scp.

        payload = optional payload
        expect  = optional list of expected response codes
        datalen = number of data bytes to read before response packet
        data    = data to send

        If datalen > 0, return data. Otherwise, return response."""
        cksum = (0x4A + cmd + len(payload)) & 0xFF
        for x in payload:
            cksum = (cksum + x) & 0xFF
        pkt = bytes([cmd, len(payload)]) + payload + bytes([cksum])
        self._port.write(pkt)
        if not data is None:
            self._port.write(data)
        if datalen > 0:
            data = self._port.read(datalen)
            self._getrsp(cmd, expect)
            return data
        else:
            return self._getrsp(cmd, expect)

    
    def select(self, drive='A'):
        """Select specified drive.

        drive = 'A' or 'B'"""
        if drive == 'A':
            self._sendcmd(CMD_SELA)
            self._sendcmd(CMD_MTRAON)
        elif drive == 'B':
            self._sendcmd(CMD_SELB)
            self._sendcmd(CMD_MTRBON)
        else:
            raise ValueError('Invalid drive letter \'{:s}\'.'.format(str(drive)))


    def deselect(self, drive='A'):
        """Deselect specified drive.

        drive = 'A' or 'B'"""
        if drive == 'A':
            self._sendcmd(CMD_DSELA)
            self._sendcmd(CMD_MTRAOFF)
        elif drive == 'B':
            self._sendcmd(CMD_DSELB)
            self._sendcmd(CMD_MTRBOFF)
        else:
            raise ValueError('Invalid drive letter \'{:s}\'.'.format(str(drive)))


    def select_side(self, side):
        """Select side. Drive must already be selected.

        side = 0 (bottom) or 1 (top)"""

        if side in [0,1]:
            self._sendcmd(CMD_SIDE, bytes([side]))
        else:
            raise ValueError('Invalid side number.')


    def read_track(self, trknum, nrevs=2, rpm=300, ignoreindex=False):
        """Read the current track. Drive and head must already be selected.

        trknum      = track number
        nrevs       = number of revolutions to read (1-5)
        rpm         = rpm of drive (300 or 360)
        ignoreindex = if True, ignore index pulses"""

        # Validate and interpret arguments
        if not ignoreindex:
            flags = 0x01
        else:
            flags = 0x00
        if rpm == 360:
            flags = flags | 0x08
        if nrevs not in scp.limits['nrevs']:
            raise ValueError('Requested number of revolutions not supported by SCP hardware.')
        
        # Perform the track read(s)
        self._sendcmd(CMD_READFLUX, bytes([nrevs, flags]))

        # Request details about the flux data,
        # then read array of five (index time, number of bitcells) pairs.
        self._sendcmd(CMD_GETFLUXINFO, bytes())
        raw_fluxinfo = be32tbl(self._port.read(4 * 2 * 5))
        fluxinfo = []
        for n in range(5):
            idxtime  = raw_fluxinfo[n*2]
            bitcells = raw_fluxinfo[(n*2)+1]
            fluxinfo.append(SCPfile.FluxInfo(idxtime=idxtime, bitcells=bitcells))

        # Add up numbers of bitcells (2 bytes each) to get total length
        fluxlen = 0
        for rev in fluxinfo:
            fluxlen = fluxlen + (rev.bitcells * 2)

        # Read the flux data out of SCP RAM and convert to list of integers
        payload = bytearray([0,0,0,0])
        payload = payload + fluxlen.to_bytes(4, byteorder='big', signed=False)
        fluxdata = be16tbl(self._sendcmd(CMD_RAM2USB, payload, datalen=fluxlen))

        # Return flux data as an SCPtrack instance
        return SCPfile.SCPtrack(trknum, fluxinfo[:nrevs], fluxdata)


    def write_track(self, trkdata, rpm=300, wipe=False, ignoreindex=False):
        """Write a track. Drive and head must already be selected.

        trk         = track data as SCPtrack
        rpm         = rpm of drive (300 or 360)
        ignoreindex = if True, ignore index pulses"""

        fluxdata = bytes()
        for v in trkdata.fluxdata:
          fluxdata = fluxdata + v.to_bytes(2, byteorder='big', signed=False)

        payload = bytearray([0,0,0,0])
        datalen = len(trkdata.fluxdata) * 2
        payload = payload + datalen.to_bytes(4, byteorder='big', signed=False)
        self._sendcmd(CMD_USB2RAM, payload, datalen=0, data=fluxdata)

        fluxlen = len(trkdata.fluxdata)
        payload = fluxlen.to_bytes(4, byteorder='big', signed=False)
        if not ignoreindex:
            flags = 0x01
        else:
            flags = 0x00
        if rpm == 360:
            flags = flags | 0x08
        if wipe:
            flags = flags | 0x04
        payload = payload + bytes([flags])
        self._sendcmd(CMD_WRITEFLUX, payload, datalen=0)
        return

    def seek0(self):
        """Seek drive to cylinder 0."""
        self._sendcmd(CMD_SEEK0)
        

    def stepto(self, cyl):
        """Step drive to specified cylinder."""
        self._sendcmd(CMD_STEPTO, bytes([cyl]))
        

    def clean(self, drive=scp.defaults['drive'], endcyl=scp.defaults['endcyl'],
                passes=scp.defaults['cleanpasses']):
        """Perform cleaning cycle, seeking between cylinders 0 and endcyl."""
        self.select(drive)
        self.seek0()
        for n in range(passes):
            for c in range(0, endcyl+1):
                self.stepto(c)
            for c in range(endcyl-1, 0, -1):
                self.stepto(c)
        self.stepto(int(endcyl/2))
        self.deselect(drive)


    def ramtest(self):
        """Perform SCP RAM test.

        Returns:
          True if test passed
          False if test failed"""
        r = self._sendcmd(CMD_RAMTEST, expect=[PR_OK, PR_BADRAM])
        if r == PR_OK:
            return True
        elif r == PR_BADRAM:
            return False
        else:
            raise RuntimeError('BUG! Impossible response code from _sendcmd(CMD_RAMTEST).')


    def version_num(self):
        """Get version numbers.

        Returns:
          tuple of (encoded hardware version number, encoded firmware version number)."""
        r = self._sendcmd(CMD_SCPINFO, expect=[PR_OK])
        if r == PR_OK:
            v = self._port.read(2)
            if len(v) != 2:
                raise RuntimeError('Invalid version {:s} from CMD_SCPINFO.'.format(str(v)))
            return (v[0], v[1])
        else:
            raise RuntimeError('BUG! Impossible response code from _sendcmd(CMD_SCPINFO).')

    def version_str(self):
        """Get SCP hardware and firmware versions.

        Returns:
          Printable string."""
        v = self.version_num()
        hv = v[0] >> 4
        hr = v[0] & 0x0F
        fv = v[1] >> 4
        fr = v[1] & 0x0F
        return 'SuperCard Pro Hardware v{:d}.{:d}, Firmware v{:d}.{:d}'.format(hv, hr, fv, fr)

