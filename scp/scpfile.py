#!/usr/bin/env python3
#
##########################################################################
# Copyright (C) 2019 Mark J. Blair, NF6X
#
# This file is part of pySuperCardPro.
#
#  pySuperCardPro is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pySuperCardPro is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pySuperCardPro.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""SuperCard Pro .scp file support."""

from collections import namedtuple
import copy
import datetime
import math
import os
import time
import warnings
import scp
from scp.util import *

# Misc. constants
FOOTER_LEN = 48


class SCPfile:
    """Class representing a flux image read from or writable to a .scp file.

    Note that reading an arbitrary .scp file into this class and then writing
    it back out again does not produce an identical file."""

    # Flags for FLAGS byte at offset 0x08
    FLAG_INDEX  = 0x01           # Set if index mark used for track sync
    FLAG_TPI    = 0x02           # Set for 96 TPI, clear for 48 TPI
    FLAG_RPM    = 0x04           # Set for 360 RPM, clear for 300 RPM
    FLAG_TYPE   = 0x08           # Set for normalized flux timing
    FLAG_MODE   = 0x10           # Set for read/write
    FLAG_FOOTER = 0x20           # Set if footer is present

    # Head numbers
    HEAD_BOTH   = 0
    HEAD_BOTTOM = 1
    HEAD_TOP    = 2

    flag_names = {
        FLAG_INDEX:  'INDEX',
        FLAG_TPI:    '96TPI',
        FLAG_RPM:    '360RPM',
        FLAG_TYPE:   'NORMALIZED',
        FLAG_MODE:   'READWRITE',
        FLAG_FOOTER: 'FOOTER'
    }

    head_names = {
        HEAD_BOTH:   "BOTH",
        HEAD_BOTTOM: "BOTTOM",
        HEAD_TOP:    "TOP"
    }

    # A namedtuple to store parameters of a single revolution of flux data:
    #   idxtime:  period of this revolution, in units of 25ns
    #   bitcells: number of flux transition time deltas recorded
    FluxInfo = namedtuple('FluxInfo', ['idxtime', 'bitcells'])


    class SCPtrack:
        """Class representing the flux image of a single track."""

        def __init__(self, trknum=None, fluxinfo=[], fluxdata=[], debug=False):
            """Initialize class storing flux data for a track.

            trknum   = Track number
            fluxinfo = List of FluxInfo namedtuples describing each revolution of this track.
            fluxdata = List of bitcell delta times."""

            self.trknum   = trknum
            self.fluxinfo = fluxinfo
            self.fluxdata = fluxdata
            self.debug    = debug


        def track_len(self):
            """Return number of bytes needed for track data in .scp file."""
            trklen = 3          # 'TRK' header
            trklen = trklen + 1 # track number byte
            for n in range(len(self.fluxinfo)):
                # Add 12 bytes per revolution for 3 longwords to be stored
                trklen = trklen + 12

            # 2 bytes per bitcell
            trklen = trklen + (len(self.fluxdata) * 2)
            return trklen


        def read_bytes(self, nrevs, fp):
            """Read track data from file"""

            trkstart = fp.tell()
            if self.debug:
                # Print detailed debugging information while we load the track
                print('      SCPtrack.read_bytes(nrevs={:d}) at offset 0x{:08X}'.format(nrevs, trkstart), flush=True)
            buf = fp.read(3)
            if buf != b'TRK':
                raise ValueError('Track magic number not found: offset=0x{:08X} read {:s}'.format(trkstart, str(buf)))
            self.trknum   = fp.read(1)[0]
            if self.debug:
                # Print detailed debugging information while we load the track
                print('        TRK {:d}'.format(self.trknum), flush=True)
            self.fluxinfo = []
            self.fluxdata = []

            for n in range(nrevs):
                # Read (idxlen, bitcells, offset) from table entry
                buf = fp.read(12)
                (idxtime, bitcells, offset) = le32tbl(buf)
                if self.debug:
                    # Print detailed debugging information while we load the track
                    print('        {:d} idxtime={:d} bitcells={:d} offset=0x{:08X}'.format(n, idxtime, bitcells, offset), flush=True)
                rev = SCPfile.FluxInfo(idxtime=idxtime, bitcells=bitcells)
                self.fluxinfo.append(rev)

                # Remember location of next table entry
                rewind = fp.tell()

                # Read flux data for this revolution
                if self.debug:
                    # Print detailed debugging information while we load the track
                    print('          read {:d} bytes at offset 0x{:08X}'.format(bitcells*2, trkstart+offset), flush=True)
                fp.seek(trkstart + offset)
                buf = fp.read(bitcells * 2)
                self.fluxdata = self.fluxdata + be16tbl(buf)

                # Rewind back to next table entry
                fp.seek(rewind)


        def write_bytes(self, fp):
            """Write track data to a file."""

            # Track data header
            fp.write(b'TRK')
            fp.write(bytes([self.trknum]))
            offset = 4 + 12 * len(self.fluxinfo)

            # Revolution data
            for rev in self.fluxinfo:
                fp.write(rev.idxtime.to_bytes(4, byteorder='little', signed=False))
                fp.write(rev.bitcells.to_bytes(4, byteorder='little', signed=False))
                fp.write(offset.to_bytes(4, byteorder='little', signed=False))
                offset = offset + (rev.bitcells * 2)

            # Flux data
            for d in self.fluxdata:
                fp.write(d.to_bytes(2, byteorder='big', signed=False))


        def __str__(self):
            """Cast to string to get a description of this track."""
            # TO DO: Improve this
            return str(self.fluxinfo)


    def __init__(self, orig=None, filename=None, debug=False):
        """Class initializer.

        Normal use cases:
        1) Call with no arguments to make a new, empty instance.
        2) Pass another SCPfile instance in orig argument to copy it.
           All other arguments are ignored if orig != None.
        3) Pass a filename in filename argument to load from a file."""
        
        self.debug=debug

        if orig is not None:
            ############################################
            # Copy another SCPfile instance. All other
            # arguments are ignored.
            self.version     = orig.version
            self.disktype    = orig.disktype
            self.numrevs     = orig.numrevs
            self.start_track = orig.start_track
            self.end_track   = orig.end_track 
            self.flags       = orig.flags
            self.bitwidth    = orig.bitwidth
            self.headnum     = orig.headnum
            self.resolution  = orig.resolution

            self.tracks = []
            for t in orig.tracks:
                self.tracks.append(copy.deepcopy(t))

            self.drive_mfg        = orig.drive_mfg
            self.drive_model      = orig.drive_model
            self.drive_sernum     = orig.drive_sernum
            self.user             = orig.user
            self.application      = orig.application
            self.comment          = orig.comment
            self.created          = orig.created
            self.modified         = orig.modified
            self.app_version      = orig.app_version
            self.scp_hw_version   = orig.scp_hw_version
            self.scp_fw_version   = orig.scp_fw_version
            self.scp_file_version = orig.scp_file_version
        
        elif filename is not None:
            ############################################
            # Initialize from a .scp file.
            # All other arguments are ignored.
            self.read_file(filename)

        else:
            ############################################
            # Create a new, empty image

            ################
            # Header details
            self.version     = scp.defaults['scpversion']
            self.disktype    = 0x33
            self.numrevs     = scp.defaults['nrevs']
            self.start_track = scp.defaults['startcyl']
            self.end_track   = (scp.defaults['endcyl'] * 2) + 1
            self.flags       = SCPfile.FLAG_FOOTER
            self.bitwidth    = 0
            self.headnum     = SCPfile.HEAD_BOTH
            self.resolution  = 0

            ################
            # Track table
            num_tracks = (max(scp.limits['cyl'])+1) * 2
            self.tracks = [None] * num_tracks

            ################
            # Footer details
            self.drive_mfg        = None
            self.drive_model      = None
            self.drive_sernum     = None
            self.user             = None
            self.application      = scp.__pkgname__
            self.comment          = None
            self.created          = round(time.time())
            self.modified         = self.created
            self.app_version      = scp.__encversion__
            self.scp_hw_version   = 0x00
            self.scp_fw_version   = 0x00
            self.scp_file_version = scp.defaults['scpversion']
            

    def _read_scp_string(self, fp, offset):
        """Helper function: Read string from .scp file."""

        if self.debug:
            # Print detailed debugging information while we load the file
            print('    _read_scp_string(0x{:08X})'.format(offset), flush=True)
        if offset == 0:
            return None
        else:
            fp.seek(offset)
            strlen  = int().from_bytes(fp.read(2), byteorder='little', signed=False)
            if self.debug:
                # Print detailed debugging information while we load the file
                print('      len = {:d}'.format(strlen), flush=True)
            strdata = fp.read(strlen).decode('utf8')
            if self.debug:
                # Print detailed debugging information while we load the file
                print('      data = \'{:s}\''.format(str(strdata)), flush=True)
            return strdata


    def read_file(self, filename):
        """Load .scp file."""

        if self.debug:
            # Print detailed debugging information while we load the file
            print('SCPfile.readfile({:s})'.format(filename), flush=True)

        with open(filename, 'rb') as fp:

            ################
            # File header
            if self.debug:
                # Print detailed debugging information while we load the file
                print('  reading header', flush=True)
            buf = fp.read(0x10)
            if len(buf) != 0x10:
                raise EOFError('Unable to read SCP file header.')
            if buf[0:3] != b'SCP':
                raise ValueError('SCP magic number not found.')

            if self.debug:
                # Print detailed debugging information while we load the file
                print('    magic       {:s}'.format(str(buf[0:3])))

            self.version     = int(buf[0x03])
            self.disktype    = int(buf[0x04])
            self.numrevs     = int(buf[0x05])
            self.start_track = int(buf[0x06])
            self.end_track   = int(buf[0x07])
            self.flags       = int(buf[0x08])
            self.bitwidth    = int(buf[0x09])
            self.headnum     = int(buf[0x0A])
            self.resolution  = int(buf[0x0B])

            if self.debug:
                # Print detailed debugging information while we load the file
                print('    version     0x{:02X}'.format(self.version))
                print('    disktype    0x{:02X}'.format(self.disktype))
                print('    numrevs     0x{:02X}'.format(self.numrevs))
                print('    start_track 0x{:02X}'.format(self.start_track))
                print('    end_track   0x{:02X}'.format(self.end_track))
                print('    flags       0x{:02X}'.format(self.flags))
                print('    bitwidth    0x{:02X}'.format(self.bitwidth))
                print('    headnum     0x{:02X}'.format(self.headnum))
                print('    resolution  0x{:02X}'.format(self.resolution))


            # Calculate checksum of rest of file
            cksum_hdr = int().from_bytes(buf[0x0C:0x10], byteorder='little', signed=False)
            if self.debug:
                # Print detailed debugging information while we load the file
                print('    checksum    0x{:08X}'.format(cksum_hdr))
                print('  reading remainder of file to calculate checksum', flush=True)
            cksum_calc = 0
            for c in fp.read():
                cksum_calc = (cksum_calc + c) % 0x100000000
            if self.debug:
                # Print detailed debugging information while we load the file
                print('    checksum    0x{:08X}'.format(cksum_calc))
                print('  rewind to end of header', flush=True)
            fp.seek(0x10)
            if cksum_hdr != cksum_calc:
                warnings.warn('Bad checksum')

            ################
            # Track offset table
            if self.debug:
                # Print detailed debugging information while we load the file
                print('  reading track offset table', flush=True)
            num_tracks = (max(scp.limits['cyl'])+1) * 2
            buf = fp.read(num_tracks * 4)
            if len(buf) != (num_tracks * 4):
                raise ValueError('Unable to read track offset table.')
            trktbl = le32tbl(buf)
            if self.debug:
                # Print detailed debugging information while we load the file
                print('    trk offset', flush=True)
                for n in range(num_tracks):
                    print('    {:<3d} 0x{:08X}'.format(n, trktbl[n]), flush=True)

            ################
            # Track flux data

            if self.debug:
                # Print detailed debugging information while we load the file
                print('  reading track flux data')
            self.tracks = [None] * num_tracks
            for trknum in range(len(trktbl)):
                if trktbl[trknum] == 0:
                    self.tracks[trknum] = None
                else:
                    if self.debug:
                        # Print detailed debugging information while we load the file
                        print('    reading track {:d} at offset 0x{:08X}'.format(trknum, trktbl[trknum]), flush=True)
                    fp.seek(trktbl[trknum])
                    trk = SCPfile.SCPtrack(debug=self.debug)
                    trk.read_bytes(self.numrevs, fp)
                    self.tracks[trknum] = trk


            ################
            # Footer

            if self.flags & SCPfile.FLAG_FOOTER:
                if self.debug:
                    # Print detailed debugging information while we load the file
                    print('  reading footer magic number', flush=True)
                # Read footer
                fp.seek(-4, os.SEEK_END)
                buf = fp.read(4)
                if self.debug:
                    # Print detailed debugging information while we load the file
                    print('    magic = {:s}'.format(str(buf)), flush=True)
                if buf != b'FPCS':
                    raise ValueError('Footer magic number not found: read {:s}.'.format(str(buf)))

                # If magic number found, seek to expected beginning of footer and read it all in
                if self.debug:
                    # Print detailed debugging information while we load the file
                    print('  reading footer', flush=True)
                fp.seek(-FOOTER_LEN, os.SEEK_END)
                buf = fp.read(FOOTER_LEN)
                if len(buf) != FOOTER_LEN:
                    # This really shouldn't happen if we get this far
                    raise RuntimeError('Unable to read footer. This should not happen.')
                
                # Decode the footer
                manufacturer_offset   = int().from_bytes(buf[0x00:0x04], byteorder='little', signed=False)
                model_offset          = int().from_bytes(buf[0x04:0x08], byteorder='little', signed=False)
                sernum_offset         = int().from_bytes(buf[0x08:0x0C], byteorder='little', signed=False)
                user_offset           = int().from_bytes(buf[0x0C:0x10], byteorder='little', signed=False)
                application_offset    = int().from_bytes(buf[0x10:0x14], byteorder='little', signed=False)
                comment_offset        = int().from_bytes(buf[0x14:0x18], byteorder='little', signed=False)
                self.created          = int().from_bytes(buf[0x18:0x20], byteorder='little', signed=False)
                self.modified         = int().from_bytes(buf[0x20:0x28], byteorder='little', signed=False)
                self.app_version      = buf[0x28]
                self.scp_hw_version   = buf[0x29]
                self.scp_fw_version   = buf[0x2A]
                self.scp_file_version = buf[0x2B]

                if self.debug:
                    # Print detailed debugging information while we load the file
                    print('    manufacturer_offset = 0x{:08X}'.format(manufacturer_offset))
                    print('    model_offset        = 0x{:08X}'.format(model_offset))
                    print('    sernum_offset       = 0x{:08X}'.format(sernum_offset))
                    print('    user_offset         = 0x{:08X}'.format(user_offset))
                    print('    application_offset  = 0x{:08X}'.format(application_offset))
                    print('    comment_offset      = 0x{:08X}'.format(comment_offset))
                    print('    created             = 0x{:016X}'.format(self.created))
                    print('    modified            = 0x{:016X}'.format(self.modified))
                    print('    app_version         = 0x{:02X}'.format(self.app_version))
                    print('    scp_hw_version      = 0x{:02X}'.format(self.scp_hw_version))
                    print('    scp_fw_version      = 0x{:02X}'.format(self.scp_fw_version))
                    print('    scp_file_version    = 0x{:02X}'.format(self.scp_file_version))

                # Load the strings (returns None when called with 0 offset)
                self.drive_mfg    = self._read_scp_string(fp, manufacturer_offset)
                self.drive_model  = self._read_scp_string(fp, model_offset)
                self.drive_sernum = self._read_scp_string(fp, sernum_offset)
                self.user         = self._read_scp_string(fp, user_offset)
                self.application  = self._read_scp_string(fp, application_offset)
                self.comment      = self._read_scp_string(fp, comment_offset)

            else:
                # Footer flag is not set, so create empty footer info
                if self.debug:
                    # Print detailed debugging information while we load the file
                    print('  creating default footer', flush=True)
                self.drive_mfg        = None
                self.drive_model      = None
                self.drive_sernum     = None
                self.user             = None
                self.application      = None
                self.comment          = None
                self.created          = 0
                self.modified         = 0
                self.app_version      = scp.__encversion__
                self.scp_hw_version   = 0x00
                self.scp_fw_version   = 0x00
                self.scp_file_version = 0x00
                

    def write_file(self, filename):
        """Save .scp file."""

        # The SCP file format includes a checksum in the header which covers
        # everything *after* the checksum. That is... not convenient.
        # We will create the new empty file, close it, reopen it read/write,
        # write out the file, and then seek back to overwrite checksum
        # padding after calculating the checksum for what we wrote.
        fp = open(filename, 'wb')
        fp.close()
        fp = open(filename, 'r+b')
        fp.seek(0)
        fp.truncate()
        
        ################
        # File header
        fp.write(b'SCP')
        fp.write(bytes([self.version, self.disktype, self.numrevs, self.start_track, self.end_track]))

        # Make sure footer flag is set
        fp.write(bytes([self.flags | SCPfile.FLAG_FOOTER]))

        fp.write(bytes([self.bitwidth, self.headnum, self.resolution]))

        # Include 4 bytes of padding for checksum
        cksum_offset = fp.tell()
        fp.write(bytes(4))
        
        ################
        # Track offset table

        # Track offset table starts at current file position.
        # Track offset table occupies 4 bytes per entry.
        # Track data starts immediately afterwards.
        num_tracks = (max(scp.limits['cyl'])+1) * 2
        offset = fp.tell() + (4 * num_tracks)

        # For each track, compute how much space it will occupy
        # and calculate its offset from the beginning of the file.
        for trk in self.tracks:
            if trk is None:
                # No track data; write 0x00000000
                fp.write(bytes(4))
            else:
                # Write file offset to this track's data
                fp.write(offset.to_bytes(4, byteorder='little', signed=False))
                offset = offset + trk.track_len()

        ################
        # Track flux data
        for trknum in range(num_tracks):
            if self.tracks[trknum] is not None:
                self.tracks[trknum].write_bytes(fp)

        ################
        # ASCII timestamp
        ts = '{:s} UTC'.format(datetime.datetime.utcfromtimestamp(self.created).isoformat())
        fp.write(ts.encode('ascii', errors='ignore'))
        fp.write(bytes(1))

        ################
        # Footer strings
        if self.drive_mfg is None:
            drive_mfg_offset = 0
        else:
            # Record offset
            drive_mfg_offset = fp.tell()
            # Write string length
            fp.write(len(self.drive_mfg).to_bytes(2, byteorder='little', signed=False))
            # Write string
            fp.write(self.drive_mfg.encode('utf8'))
            # Write trailing NUL
            fp.write(bytes(1))

        if self.drive_model is None:
            drive_model_offset = 0
        else:
            # Record offset
            drive_model_offset = fp.tell()
            # Write string length
            fp.write(len(self.drive_model).to_bytes(2, byteorder='little', signed=False))
            # Write string
            fp.write(self.drive_model.encode('utf8'))
            # Write trailing NUL
            fp.write(bytes(1))

        if self.drive_sernum is None:
            drive_sernum_offset = 0
        else:
            # Record offset
            drive_sernum_offset = fp.tell()
            # Write string length
            fp.write(len(self.drive_sernum).to_bytes(2, byteorder='little', signed=False))
            # Write string
            fp.write(self.drive_sernum.encode('utf8'))
            # Write trailing NUL
            fp.write(bytes(1))

        if self.user is None:
            user_offset = 0
        else:
            # Record offset
            user_offset = fp.tell()
            # Write string length
            fp.write(len(self.user).to_bytes(2, byteorder='little', signed=False))
            # Write string
            fp.write(self.user.encode('utf8'))
            # Write trailing NUL
            fp.write(bytes(1))

        if self.application is None:
            application_offset = 0
        else:
            # Record offset
            application_offset = fp.tell()
            # Write string length
            fp.write(len(self.application).to_bytes(2, byteorder='little', signed=False))
            # Write string
            fp.write(self.application.encode('utf8'))
            # Write trailing NUL
            fp.write(bytes(1))

        if self.comment is None:
            comment_offset = 0
        else:
            # Record offset
            comment_offset = fp.tell()
            # Write string length
            fp.write(len(self.comment).to_bytes(2, byteorder='little', signed=False))
            # Write string
            fp.write(self.comment.encode('utf8'))
            # Write trailing NUL
            fp.write(bytes(1))

        ################
        # Footer
        fp.write(drive_mfg_offset.to_bytes(4, byteorder='little', signed=False))
        fp.write(drive_model_offset.to_bytes(4, byteorder='little', signed=False))
        fp.write(drive_sernum_offset.to_bytes(4, byteorder='little', signed=False))
        fp.write(user_offset.to_bytes(4, byteorder='little', signed=False))
        fp.write(application_offset.to_bytes(4, byteorder='little', signed=False))
        fp.write(comment_offset.to_bytes(4, byteorder='little', signed=False))
        fp.write(self.created.to_bytes(8, byteorder='little', signed=False))
        fp.write(self.modified.to_bytes(8, byteorder='little', signed=False))
        fp.write(bytes([self.app_version]))
        fp.write(bytes([self.scp_hw_version]))
        fp.write(bytes([self.scp_fw_version]))
        fp.write(bytes([self.scp_file_version]))
        fp.write(b'FPCS')

        ################
        # Checksum
        cksum = 0
        fp.seek(cksum_offset+4)
        for c in fp.read():
            cksum = (cksum + c) % 0x100000000
        fp.seek(cksum_offset)
        fp.write(cksum.to_bytes(4, byteorder='little', signed=False))
        
        fp.close()


    def __str__(self):
        """Cast to string to get a printable description of the class instance."""
        
        desc = 'SCPfile instance:\n'
        desc = desc + '  Header:\n'
        desc = desc + '    version:    {:d}.{:d}\n'.format(self.version >> 4, self.version & 0x0F)
        desc = desc + '    disktype:   0x{:02X}\n'.format(self.disktype)
        desc = desc + '    numrevs:    {:d}\n'.format(self.numrevs)
        desc = desc + '    strack:     {:<3d} ({:d}.{:d})\n'.format(self.start_track, math.floor(self.start_track/2), self.start_track & 0x01)
        desc = desc + '    etrack:     {:<3d} ({:d}.{:d})\n'.format(self.end_track, math.floor(self.end_track/2), self.end_track & 0x01)
        flagstr = ''
        for f in SCPfile.flag_names.keys():
            if self.flags & f:
                flagstr = flagstr + ' ' + SCPfile.flag_names[f]
        desc = desc + '    flags:      0x{:02X}{:s}\n'.format(self.flags, flagstr)
        desc = desc + '    bitwidth:   {:d}\n'.format(self.bitwidth)
        desc = desc + '    headnum:    {:d} {:s}\n'.format(self.headnum, SCPfile.head_names[self.headnum])
        desc = desc + '    resolution: {:d}ns\n'.format((self.resolution+1) * 25)

        desc = desc + '  Tracks:\n'
        desc = desc + '    trk  (c.h)  (period, bitcells)...\n'
        for trk in range(len(self.tracks)):
            if self.tracks[trk] is not None:
                if self.headnum == SCPfile.HEAD_BOTH:
                    # Double sided image
                    tstr = '({:d}.{:d})'.format(math.floor(trk/2), trk & 0x01)
                elif self.headnum == SCPfile.HEAD_BOTTOM:
                    # Single sided image, front side
                    tstr = '({:d}.0)'.format(trk)
                elif self.headnum == SCPfile.HEAD_TOP:
                    # Single sided image, back side
                    tstr = '({:d}.1)'.format(trk)
                desc = desc + '    {:3d} {:>6s}:'.format(trk, tstr)
                trkper = 0
                for rev in self.tracks[trk].fluxinfo:
                    desc = desc + ' ({:f}ms, {:d})'.format((rev.idxtime * 25)/1E6, rev.bitcells)
                    trkper = trkper + rev.idxtime * 25E-9
                trkper = trkper / len(self.tracks[trk].fluxinfo)
                rpm = 60/trkper
                desc = desc + ' {:.3f} RPM\n'.format(rpm)

        desc = desc + '  Footer:\n'
        if self.drive_mfg is not None:
            desc = desc + '    manufacturer:         \'{:s}\'\n'.format(self.drive_mfg)
        if self.drive_model is not None:
            desc = desc + '    model:                \'{:s}\'\n'.format(self.drive_model)
        if self.drive_sernum is not None:
            desc = desc + '    serial number:        \'{:s}\'\n'.format(self.drive_sernum)
        if self.user is not None:
            desc = desc + '    user:                 \'{:s}\'\n'.format(self.user)
        if self.application is not None:
            desc = desc + '    application:          \'{:s}\'\n'.format(self.application)
        if self.comment is not None:
            desc = desc + '    comment:              \'{:s}\'\n'.format(self.comment)
        desc = desc + '    created:              {:s} UTC\n'.format(datetime.datetime.utcfromtimestamp(self.created).isoformat())
        desc = desc + '    modified:             {:s} UTC\n'.format(datetime.datetime.utcfromtimestamp(self.modified).isoformat())
        desc = desc + '    application version:  {:d}.{:d}\n'.format(self.app_version >> 4, self.app_version & 0x0F)
        desc = desc + '    SCP hardware version: {:d}.{:d}\n'.format(self.scp_hw_version >> 4, self.scp_hw_version & 0x0F)
        desc = desc + '    SCP firmware version: {:d}.{:d}\n'.format(self.scp_fw_version >> 4, self.scp_fw_version & 0x0F)
        desc = desc + '    SCP file version:     {:d}.{:d}\n'.format(self.scp_file_version >> 4, self.scp_file_version & 0x0F)

        return desc
