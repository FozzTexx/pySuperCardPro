#!/usr/bin/env python3
#
##########################################################################
# Copyright (C) 2019 Mark J. Blair, NF6X
#
# This file is part of pySuperCardPro.
#
#  pySuperCardPro is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pySuperCardPro is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pySuperCardPro.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""Command functions for scputil script."""

from collections import namedtuple
import sys
import textwrap
import time
import scp
from scp.util import *


def cmd_clean(args):
    """Seek heads to wipe them with a cleaning diskette."""
    if len(args.cmdargs) == 0:
        passes = scp.defaults['cleanpasses']
    elif len(args.cmdargs) == 1:
        passes = int(args.cmdargs[0], 0)
    else:
        raise ValueError('Unexpected argument to clean command.')
    if passes <= 0:
        raise ValueError('Optional pass count argument to clean command must be > 0.')

    dev = scp.SCPdev(args.port)
    dev.clean(args.drive, args.endcyl, passes)


def cmd_edit(args):
    """Edit a .scp file, replacing metadata."""
    if len(args.cmdargs) != 2:
        raise ValueError('edit command requires 2 arguments: INFILE OUTFILE.')

    scpfile = scp.SCPfile(filename=args.cmdargs[0], debug=args.debug)

    if args.comment is not None:
        scpfile.comment = args.comment

    if args.drive_mfg is not None:
        scpfile.drive_mfg = args.drive_mfg

    if args.drive_model is not None:
        scpfile.drive_model = args.drive_model

    if args.drive_sernum is not None:
        scpfile.drive_sernum = args.drive_sernum

    if args.user is not None:
        scpfile.user = args.user

    scpfile.modified = round(time.time())

    scpfile.write_file(args.cmdargs[1])


def cmd_help(args):
    """Print help for a command."""
    if len(args.cmdargs) != 1:
        raise ValueError('help command requires 1 argument: COMMAND. Maybe try the --help option?')
    if args.cmdargs[0] not in cmdtable:
        raise ValueError('Unknown command. Valid commands are {:s}.'.format(str(list(cmdtable.keys()))))
    print(cmdtable[args.cmdargs[0]].example)
    print(indent(cmdtable[args.cmdargs[0]].doc, 2))


def cmd_info(args):
    """Print detailed information about a .scp file."""
    if len(args.cmdargs) != 1:
        raise ValueError('info command requires 1 argument: INFILE.')
    scpfile = scp.SCPfile(filename=args.cmdargs[0], debug=args.debug)
    print(scpfile)


def cmd_ramtest(args):
    """Test the SuperCard Pro device RAM."""
    if len(args.cmdargs) != 0:
        raise ValueError('ramtest command does not accept any arguments.')
    dev = scp.SCPdev(args.port)
    if dev.ramtest():
        print('SuperCard Pro RAM test PASSED.')
        return 0
    else:
        return 'SuperCard Pro RAM test FAILED.'


def cmd_read(args):
    """Read a diskette and save its flux image to a .scp file."""
    if len(args.cmdargs) != 1:
        raise ValueError('info command requires 1 argument: OUTFILE.')
    filename = args.cmdargs[0]

    dev = scp.SCPdev(args.port)

    # Create a new SCPfile instance
    scpfile = scp.SCPfile(debug=args.debug)

    scpfile.numrevs = args.nrevs

    if args.side == 0:
        # Single sided image of bottom (front) side
        sides = [0]
        scpfile.start_track = args.startcyl
        scpfile.end_track   = args.endcyl
        scpfile.headnum     = scp.scpfile.SCPfile.HEAD_BOTTOM
        ss_flag             = True
    elif args.side == 1:
        # Single sided image of top (back) side
        sides = [1]
        scpfile.start_track = args.startcyl
        scpfile.end_track   = args.endcyl
        scpfile.headnum     = scp.scpfile.SCPfile.HEAD_TOP
        ss_flag             = True
    elif args.side == 2:
        # Image both sides
        sides = [0,1]
        scpfile.start_track = args.startcyl * 2
        scpfile.end_track   = (args.endcyl * 2) + 1
        scpfile.headnum     = scp.scpfile.SCPfile.HEAD_BOTH
        ss_flag             = False
    else:
        raise ValueError('Impossible side argument.')

    
    # Set up flags
    scpfile.flags = scp.scpfile.SCPfile.FLAG_FOOTER
    if not args.ignoreindex:
        scpfile.flags = scpfile.flags | scp.scpfile.SCPfile.FLAG_INDEX
    if args.tpi == 96:
        scpfile.flags = scpfile.flags | scp.scpfile.SCPfile.FLAG_TPI
    if args.rpm == 360:
        scpfile.flags = scpfile.flags | scp.scpfile.SCPfile.FLAG_RPM

    # Get SCP hardware and firmware versions
    v = dev.version_num()
    scpfile.scp_hw_version = v[0]
    scpfile.scp_fw_version = v[1]

    # Set up optional strings
    if args.comment is not None:
        scpfile.comment = args.comment
    if args.drive_mfg is not None:
        scpfile.drive_mfg = args.drive_mfg
    if args.drive_model is not None:
        scpfile.drive_model = args.drive_model
    if args.drive_sernum is not None:
        scpfile.drive_sernum = args.drive_sernum
    if args.user is not None:
        scpfile.user = args.user

    # Read the disk
    dev.select(drive=args.drive)

    if not args.suppress0seek:
        # Seek to track 0 to make sure that SCP hardware is in sync
        # with the current head position... unless the user asked
        # us to not do this (for example, to keep the heads from picking
        # up oxide from damaged lower tracks). If the user suppressed
        # the seek to track 0, then it's up to them to get the SCP
        # hardware in sync with the head position before this imaging
        # operation is started.
        if args.verbose:
            print('Seek to track 0')
        dev.seek0()

    for cyl in range(args.startcyl, args.endcyl+1):
        # Move heads to cylinder
        dev.stepto(cyl)
        for side in sides:
            if args.verbose:
                print('Reading {:d}.{:d}'.format(cyl, side))

            if ss_flag:
                trknum = cyl
            else:
                trknum = (cyl * 2) + side

            # Select the desired head
            dev.select_side(side)

            # Read the track
            try:
                trk = dev.read_track(trknum, args.nrevs, args.rpm, args.ignoreindex)
            except:
                # Something went wrong! Try writing out what we have already read.
                print('ERROR! Trying to write partial .scp file...', file=sys.stderr)
                scpfile.write_file(filename)
                dev.deselect(drive=args.drive)
                raise

            # Insert track data into the SCPfile instance
            scpfile.tracks[trknum] = trk
            
    dev.deselect(drive=args.drive)

    # Write out the file
    if args.verbose:
        print(scpfile)
        print('Writing flux data to \'{:s}\''.format(filename))
    scpfile.write_file(filename)



def cmd_seek(args):
    """Seek to cylinder 0, then optionally step heads to specified cylinder."""
    if len(args.cmdargs) == 0:
        cyl = 0
    elif len(args.cmdargs) == 1:
        cyl = int(args.cmdargs[0], 0)
    else:
        raise ValueError('Unexpected argument to seek command.')
    if cyl < 0:
        raise ValueError('Optional cylinder number argument to seek command must be >= 0.')

    dev = scp.SCPdev(args.port)
    dev.select(drive=args.drive)
    if not args.suppress0seek:
        dev.seek0()
    if cyl > 0:
        dev.stepto(cyl)
    dev.deselect(drive=args.drive)


def cmd_version(args):
    """Print SuperCard Pro hardware and firmware versions."""
    if len(args.cmdargs) != 0:
        raise ValueError('version command does not accept any arguments.')
    dev = scp.SCPdev(args.port)
    print(dev.version_str())
    return 0


def cmd_write(args):
    """Write flux image from a .scp file to a diskette."""
    if len(args.cmdargs) != 1:
        raise ValueError('info command requires 1 argument: INFILE.')
    dev = scp.SCPdev(args.port)
    scpfile = scp.SCPfile(filename=args.cmdargs[0], debug=args.debug)

    if args.side == 0:
        # Single sided image of bottom (front) side
        sides = [0]
        scpfile.start_track = args.startcyl
        scpfile.end_track   = args.endcyl
        scpfile.headnum     = scp.scpfile.SCPfile.HEAD_BOTTOM
        ss_flag             = True
    elif args.side == 1:
        # Single sided image of top (back) side
        sides = [1]
        scpfile.start_track = args.startcyl
        scpfile.end_track   = args.endcyl
        scpfile.headnum     = scp.scpfile.SCPfile.HEAD_TOP
        ss_flag             = True
    elif args.side == 2:
        # Image both sides
        sides = [0,1]
        scpfile.start_track = args.startcyl * 2
        scpfile.end_track   = (args.endcyl * 2) + 1
        scpfile.headnum     = scp.scpfile.SCPfile.HEAD_BOTH
        ss_flag             = False
    else:
        raise ValueError('Impossible side argument.')

    # Read the disk
    dev.select(drive=args.drive)

    if not args.suppress0seek:
        # Seek to track 0 to make sure that SCP hardware is in sync
        # with the current head position... unless the user asked
        # us to not do this (for example, to keep the heads from picking
        # up oxide from damaged lower tracks). If the user suppressed
        # the seek to track 0, then it's up to them to get the SCP
        # hardware in sync with the head position before this imaging
        # operation is started.
        if args.verbose:
            print('Seek to track 0')
        dev.seek0()

    for cyl in range(args.startcyl, args.endcyl+1):
        # Move heads to cylinder
        dev.stepto(cyl)
        for side in sides:
            if args.verbose:
                print('Writing {:d}.{:d}'.format(cyl, side))

            if ss_flag:
                trknum = cyl
            else:
                trknum = (cyl * 2) + side

            # Select the desired head
            dev.select_side(side)

            if scpfile.tracks[trknum] is None:
                continue
              
            # Write the track
            try:
                dev.write_track(scpfile.tracks[trknum], args.rpm, args.ignoreindex)
            except:
                # Something went wrong!
                print('ERROR! Unable to write track.', file=sys.stderr)
                dev.deselect(drive=args.drive)
                raise
            
    dev.deselect(drive=args.drive)
    return 0



# Dictionary of supported commands and the functions that implement them
CmdTableEntry = namedtuple('CmdTableEntry', ['fn', 'example', 'doc'])

cmdtable = {
    'clean':   CmdTableEntry(fn=cmd_clean,   example='clean [PASSES]', doc=textwrap.dedent("""\
                   Wipe heads back and forth over a cleaning diskette (which should be
                   inserted in the drive before executing this command). Optional PASSES
                   argument specifies how many times to move the heads back and forth.""")),

    'edit':    CmdTableEntry(fn=cmd_edit,    example='edit INFILE OUTFILE', doc=textwrap.dedent("""\
                   Read in a *.scp file, make edits such as changing the file comment,
                   and then write it out. If OUTFILE is not specified, overwrite the original
                   file.""")),

    'help':    CmdTableEntry(fn=cmd_help,    example='help COMMAND', doc=textwrap.dedent("""\
                   Print documentation for an scputil command.""")),

    'info':    CmdTableEntry(fn=cmd_info,    example='info INFILE', doc=textwrap.dedent("""\
                   Print detailed information about a *.scp file.""")),

    'ramtest': CmdTableEntry(fn=cmd_ramtest, example='ramtest', doc=textwrap.dedent("""\
                   Perform a test of the SuperCard Pro buffer memory.""")),

    'read':    CmdTableEntry(fn=cmd_read,    example='read OUTFILE', doc=textwrap.dedent("""\
                   Read flux data from a diskette and save it to specified *.scp file.""")),

    'seek':    CmdTableEntry(fn=cmd_seek,    example='seek [CYLINDER]', doc=textwrap.dedent("""\
                   Seek heads to cylinder 0, and then optionally step them to specified
                   cylinder number.""")),

    'version': CmdTableEntry(fn=cmd_version, example='version', doc=textwrap.dedent("""\
                   Print the SuperCard Pro hardware and firmware version numbers.""")),

    'write':   CmdTableEntry(fn=cmd_write,   example='write INFILE', doc=textwrap.dedent("""\
                   Write flux data from specified *.scp file to a diskette."""))
}
