#!/usr/bin/env python3
#
##########################################################################
# Copyright (C) 2019-2020 Mark J. Blair, NF6X
#
# This file is part of pySuperCardPro.
#
#  pySuperCardPro is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pySuperCardPro is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pySuperCardPro.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""Package for controlling SuperCard Pro hardware and manipulating .scp files."""

# Package details, mostly for internal use
__all__        = ['scpfile', 'scpdev']
__pkgname__    = 'pySuperCardPro'
__encversion__ = 0x02
__version__    = '{:d}.{:d}'.format(__encversion__ >> 4, __encversion__ & 0x0F) 
__copyright__  = 'Copyright (C) 2019-2020 Mark J. Blair, released under GPLv3'
__pkg_url__    = 'https://gitlab.com/NF6X_Retrocomputing/pySuperCardPro'
__dl_url__     = 'https://gitlab.com/NF6X_Retrocomputing/pySuperCardPro'

# Defaults, mostly for internal use
defaults = {
    'baud':        250000,                # SCP baud rate
    'cleanpasses': 5,                     # Cleaning cycles passes
    'drive':       'A',                   # Drive letter
    'endcyl':      79,                    # End cylinder
    'nrevs':       1,                     # Number of revolutions to image
    'port':        '/dev/cu.usbserial-SCP_JIM',
    'rpm':         300,                   # RPM of drive
    'scpversion':  0x19,                  # .scp file version = 1.9
    'side':        2,                     # Image both sides
    'startcyl':    0,                     # Start cylinder 0
    'tpi':         96                     # 96 tracks per inch
}

# Limits, mostly for internal use
limits = {
    'cyl':         range(0,83),
    'drive':       ['A', 'B'],
    'nrevs':       range(1,6),
    'rpm':         [300, 360],
    'tpi':         [48, 96]
}

# Make primary API classes available in the SCP namespace for end users
from .scpfile import SCPfile
from .scpdev  import SCPdev
