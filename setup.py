#!/usr/bin/env python3
#
##########################################################################
# Copyright (C) 2019 Mark J. Blair, NF6X
#
# This file is part of pySuperCardPro.
#
#  pySuperCardPro is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pySuperCardPro is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pySuperCardPro.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""Installation script for pySuperCardPro.

Usage examples:

    Install in your user directory:
        ./setup.py install --user

    Install in the system default location:
        sudo ./setup.py install

    Install under /usr/local:
        sudo ./setup.py install --prefix /usr/local

    In some cases, it may be necessary to set PYTHONPATH
    to point to the installation location, e.g.:
        sudo PYTHONPATH=/usr/local/lib/python3.7/site-packages ./setup.py install --prefix /usr/local
"""


from setuptools import setup
from scp import __pkgname__, __version__, __pkg_url__, __dl_url__

long_description="""pySuperCardPro includes a package and utilities for examining, creating
and manipulating SuperCard Pro (*.scp) files, and controlling the SuperCard Pro
hardware. The author of pySuperCardPro is not affiliated with the creator
of the SuperCard Pro hardware. The SuperCard Pro hardware can currently be
purchased at: http://www.cbmstuff.com/proddetail.php?prod=SCP"""


setup(name                 = __pkgname__,
      version              = __version__,
      description          = 'Python support for the SuperCard Pro diskette imaging device.',
      long_description     = long_description,
      author               = 'Mark J. Blair',
      author_email         = 'nf6x@nf6x.net',
      url                  = __pkg_url__,
      download_url         = __dl_url__,
      license              = 'GPLv3',
      packages             = ['scp'],
      provides             = ['scp', 'scputil'],
      scripts              = ['scputil'],
      include_package_data = True,
      keywords             = ['SuperCardPro', 'SuperCard Pro', 'SCP', 'scp', '.scp'],
      classifiers          = ['Development Status :: 3 - Alpha',
                              'Environment :: Console',
                              'Intended Audience :: Developers'
                              'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
                              'Natural Language :: English',
                              'Operating System :: OS Independent',
                              'Programming Language :: Python :: 3',
                              'Programming Language :: Python :: 3.7',
                              'Programming Language :: Python :: 3.8',
                              'Topic :: System :: Archiving',
                              'Topic :: System :: Emulators',
                              'Topic :: System :: Filesystems',
                              'Topic :: System :: Software Distribution',
                              'Topic :: Utilities'])
