# pySuperCardPro: Python support for the SuperCard Pro diskette imaging device.

pySuperCardPro includes a package and utilities for examining, creating and manipulating SuperCard Pro (\*.scp) files, and controlling the SuperCard Pro hardware. The author of pySuperCardPro is not affiliated with the creator of the SuperCard Pro hardware. The SuperCard Pro hardware can currently be purchased at:

    http://www.cbmstuff.com/proddetail.php?prod=SCP

**pySuperCardPro is still in an early stage of development. Don't trust it with any important data yet.**

